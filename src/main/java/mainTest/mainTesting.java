package mainTest;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import classesDAOHibernateJPA.EMFactory;
import model.Image;

public class mainTesting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = new EMFactory().getEMF();
		EntityManager em = emf.createEntityManager();
		
		Image i1= new Image();
		i1.setName("Soyunafoto");
		i1.setPath("/path");
		
		em.getTransaction().begin();
		em.persist(i1);
		em.getTransaction().commit();
		
		
	}

}
